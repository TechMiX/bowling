package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {

	arguments := os.Args[1:]
	if len(arguments) == 0 {
		l := Line{}
		l.GenerateRandomly()
		fmt.Println(l.AsString())
		return
	}

	file, err := os.Open(arguments[0])
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		l := Line{}
		for _, scoreString := range strings.Split(scanner.Text(), ", ") {
			score, err := strconv.Atoi(scoreString)
			if err != nil {
				log.Fatal(err)
			}
			l.InsertScore(score)
		}
		fmt.Println(l.AsString())
	}

}
