package main

import (
	"testing"
)

func TestFrame_IsFinished(t *testing.T) {
	tests := []struct {
		FrameNumber    int
		Scores         []int
		ExpectedResult bool
		ErrorMessage   string
	}{
		{
			FrameNumber:    1,
			Scores:         []int{2, 8},
			ExpectedResult: true,
			ErrorMessage:   "A frame (non-last) with two scores is finished",
		},
		{
			FrameNumber:    5,
			Scores:         []int{1},
			ExpectedResult: false,
			ErrorMessage:   "A frame (non-last) with one score is not finished",
		},
		{
			FrameNumber:    5,
			Scores:         []int{10},
			ExpectedResult: true,
			ErrorMessage:   "A frame (non-last) with a strike is finished",
		},
		{
			FrameNumber:    10,
			Scores:         []int{10},
			ExpectedResult: false,
			ErrorMessage:   "Last frame with one strike is not finished",
		},
		{
			FrameNumber:    10,
			Scores:         []int{10, 2},
			ExpectedResult: false,
			ErrorMessage:   "Last frame with one strike and one try is not finished",
		},
		{
			FrameNumber:    10,
			Scores:         []int{10, 10},
			ExpectedResult: false,
			ErrorMessage:   "Last frame with two strikes is not finished",
		},
	}

	for _, test := range tests {
		t.Run("Frame Finished Test", func(t *testing.T) {
			f := Frame{
				Number: test.FrameNumber,
			}
			for _, score := range test.Scores {
				f.AddScore(score)
			}
			if f.IsFinished() != test.ExpectedResult {
				t.Errorf(test.ErrorMessage+"\n%s", f.AsString())
			}
		})
	}
}

func TestFrame_AsString(t *testing.T) {
	tests := []struct {
		FrameNumber    int
		Scores         []int
		ExpectedResult string
		ErrorMessage   string
	}{
		{
			FrameNumber:    1,
			Scores:         []int{2, 8},
			ExpectedResult: "2, /",
			ErrorMessage:   "A frame (non-last) with two scores summing to 10 is a spare",
		},
		{
			FrameNumber:    1,
			Scores:         []int{2, 7},
			ExpectedResult: "2, 7",
			ErrorMessage:   "A frame (non-last) with two scores suming less than 10 is nothing!",
		},
		{
			FrameNumber:    5,
			Scores:         []int{1, 0},
			ExpectedResult: "1, -",
			ErrorMessage:   "Score of 0 (a miss) should be -",
		},
		{
			FrameNumber:    5,
			Scores:         []int{10},
			ExpectedResult: "X",
			ErrorMessage:   "Score of 10 on first try is strike and shoube be X",
		},
		{
			FrameNumber:    10,
			Scores:         []int{10, 0, 10},
			ExpectedResult: "X, -, /",
			ErrorMessage:   "Last frame with two 10 score in the first and the last try is a strike and a spare",
		},
		{
			FrameNumber:    10,
			Scores:         []int{10, 10, 0},
			ExpectedResult: "X, X, -",
			ErrorMessage:   "Last frame with two 10 and one miss is two strikes and one miss",
		},
		{
			FrameNumber:    10,
			Scores:         []int{10, 1, 9},
			ExpectedResult: "X, 1, /",
			ErrorMessage:   "Last frame with one 10 score and two score suming to 10 is one strike and one spare",
		},
		{
			FrameNumber:    10,
			Scores:         []int{10, 10, 10},
			ExpectedResult: "X, X, X",
			ErrorMessage:   "Last frame with three 10 scores is three strikes",
		},
		{
			FrameNumber:    10,
			Scores:         []int{0, 10, 10},
			ExpectedResult: "-, /, X",
			ErrorMessage:   "Last frame with the first try scores suming to 10 and last try a score of 10 is a spare and a strike",
		},
	}

	for _, test := range tests {
		t.Run("Frame String serialization test", func(t *testing.T) {
			f := Frame{
				Number: test.FrameNumber,
			}
			for _, score := range test.Scores {
				f.AddScore(score)
			}
			if f.AsString() != test.ExpectedResult {
				t.Errorf(test.ErrorMessage+"\n%s", f.AsString())
			}
		})
	}
}
