American Ten-Pin Bowling Score Calculator
-----------------------------------------

This program, given a valid sequence of rolls for one line of American Ten-Pin Bowling game, produces the total score of that game.

Input file example:
```
10, 10, 6, 2, 3, 4, 4, 6, 7, 3, 8, 2, 4, 6, 8, 1, 2, 6
```

Output:
```
| f1 | f2 | f3 | f4 | f5 | f6 | f7 | f8 | f9 | f10|
|X   |X   |6, 2|3, 4|3, 6|7, /|8, /|4, /|8, 1|2, 6|
score: 135
```

Run
-----
This program requires a `go` compiler. 
Navigate to the root of repository and then:
```
go build
```
This will generate an executable, run it like this:
```
./bowling /path/to/input/file
```

If no input is given, it will generate a random game calculates the score.

Test
----
Navigate to the root of repository and then:
```
go test
```
You should see the word `PASS`
