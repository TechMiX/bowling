package main

import (
	"strconv"
	"strings"
)

type Frame struct {
	Number   int
	Scores   []int
	ScoreSum int
}

func (f *Frame) IsFinished() bool {
	if f.Number == 10 {
		return len(f.Scores) == 3 || (len(f.Scores) == 2 && f.ScoreSum < 10)
	}

	return f.ScoreSum == 10 || len(f.Scores) == 2
}

func (f *Frame) AddScore(score int) {
	f.Scores = append(f.Scores, score)
	f.ScoreSum = f.ScoreSum + score
}

func (f *Frame) AsString() string {
	var symbols []string
	currentScoreSum := 0
	scoreIndex := -1
	for _, score := range f.Scores {
		scoreIndex++
		currentScoreSum = currentScoreSum + score
		symbol := ""
		if score == 0 {
			symbol = "-"
		} else if score == 10 && scoreIndex == 0 {
			symbol = "X"
			currentScoreSum = 0
			scoreIndex = -1
		} else if currentScoreSum == 10 {
			symbol = "/"
			currentScoreSum = 0
			scoreIndex = -1
		} else {
			symbol = strconv.Itoa(score)
		}
		symbols = append(symbols, symbol)
	}
	return strings.Join(symbols, ", ")
}
