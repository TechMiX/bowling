package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"time"
)

type Line struct {
	Frames       []*Frame
	CurrentFrame *Frame
}

func (l *Line) GenerateRandomly() {
	rand.Seed(time.Now().UnixNano())
	randomScore := 0
	for !l.IsFinished() {
		currentFrame := l.GetCurrentFrame()
		possibleScores := (11 - currentFrame.ScoreSum)
		if currentFrame.Number == 10 {
			if currentFrame.ScoreSum > 10 {
				possibleScores = currentFrame.ScoreSum - 10
			} else if currentFrame.ScoreSum == 10 && len(currentFrame.Scores) <= 2 {
				possibleScores = 11
			}
		}
		randomScore = rand.Int() % possibleScores
		l.InsertScore(randomScore)
	}
}

func (l *Line) IsFinished() bool {
	return len(l.Frames) == 10 && l.CurrentFrame.IsFinished()
}

func (l *Line) TotalScore() int {
	totalScore := 0
	for i := 0; i < len(l.Frames); i++ {
		frame := l.Frames[i]
		if frame.Number < 10 {

			// Both Spare and Strike get the next frame first score as bonus
			if frame.ScoreSum == 10 {
				totalScore = totalScore + l.Frames[i+1].Scores[0]
			}

			// If Strike, get the second score as well
			if frame.Scores[0] == 10 {
				bonusScore := 0
				if len(l.Frames[i+1].Scores) > 1 {
					bonusScore = l.Frames[i+1].Scores[1]
				} else { // If it's Strike again, look for the next frame first score!
					bonusScore = l.Frames[i+2].Scores[0]
				}
				totalScore = totalScore + bonusScore
			}
		}
		totalScore = totalScore + frame.ScoreSum
	}
	return totalScore
}

func (l *Line) AsString() string {
	scores := "|"
	titles := "|"
	for _, frame := range l.Frames {
		frameScores := fmt.Sprintf("%-4s", frame.AsString())
		scores = scores + frameScores + "|"

		titleFormatedByFrameScores := fmt.Sprintf(" f%%-%ds", len(frameScores)-2)
		titles = titles + fmt.Sprintf(titleFormatedByFrameScores, strconv.Itoa(frame.Number)) + "|"
	}
	return fmt.Sprintf("%s\n%s\nscore: %d", titles, scores, l.TotalScore())
}

func (l *Line) NewFrame() *Frame {
	newFrame := Frame{
		Number: len(l.Frames) + 1,
	}
	l.Frames = append(l.Frames, &newFrame)
	return &newFrame
}

func (l *Line) GetCurrentFrame() *Frame {
	if l.CurrentFrame == nil || (l.CurrentFrame.Number < 10 && l.CurrentFrame.IsFinished()) {
		l.CurrentFrame = l.NewFrame()
	}
	return l.CurrentFrame
}

func (l *Line) InsertScore(score int) {
	l.GetCurrentFrame().AddScore(score)
}
