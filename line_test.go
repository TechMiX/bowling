package main

import (
	"testing"
)

func TestLine_IsFinished(t *testing.T) {

	tests := []struct {
		Scores         []int
		ExpectedResult bool
		ErrorMessage   string
	}{
		{
			Scores:         []int{1, 1, 1},
			ExpectedResult: false,
			ErrorMessage:   "A Line should be considered finished only when all frames have determined score",
		},
		{
			Scores:         []int{10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10},
			ExpectedResult: true,
			ErrorMessage:   "All frames with strike is considered finished",
		},
	}

	for _, test := range tests {
		t.Run("Line Finished Test", func(t *testing.T) {
			l := Line{}
			for _, score := range test.Scores {
				l.InsertScore(score)
			}
			if l.IsFinished() != test.ExpectedResult {
				t.Errorf(test.ErrorMessage+"\n%s", l.AsString())
			}
		})
	}
}

func TestLine_GenerateRandomly(t *testing.T) {
	l := Line{}
	l.GenerateRandomly()

	if len(l.Frames) != 10 {
		t.Errorf("A Line should have 10 frames")
	}

	for _, frame := range l.Frames {
		if frame.Number < 10 && frame.ScoreSum > 10 {
			t.Errorf("The score sum of every Frame in a Line should not be bigger than 10")
		}
	}

	lastFrame := l.Frames[9]
	if len(lastFrame.Scores) > 3 {
		t.Errorf("Maximum tries of Last Frame should be 3")
	}

	if len(lastFrame.Scores) > 1 {
		sumOfFirstTwoTries := lastFrame.Scores[0] + lastFrame.Scores[1]
		if !(sumOfFirstTwoTries <= 10 || sumOfFirstTwoTries == 20) {
			t.Errorf("The score sum of the first two tries of last frame should be less than 10 or 20")
		}

		if len(lastFrame.Scores) == 3 && lastFrame.Scores[0] == 10 {
			sumOfLastTwoTries := lastFrame.Scores[1] + lastFrame.Scores[2]
			if !(sumOfLastTwoTries <= 10 || sumOfLastTwoTries == 20) {
				t.Errorf("If last frame got a strike, The score sum of the last two tries of last frame should be less than 10 or 20")
			}
		}
	}

}

func TestLine_TotalScore(t *testing.T) {
	tests := []struct {
		Scores         []int
		ExpectedResult int
	}{
		{
			Scores:         []int{5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5},
			ExpectedResult: 150,
		},
		{
			Scores:         []int{10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10},
			ExpectedResult: 300,
		},
		{
			Scores:         []int{0, 3, 5, 0, 9, 1, 2, 5, 3, 2, 4, 2, 3, 3, 4, 6, 10, 10, 2, 5},
			ExpectedResult: 103,
		},
	}

	for _, test := range tests {
		t.Run("Total Score Test", func(t *testing.T) {
			l := Line{}
			for _, score := range test.Scores {
				l.InsertScore(score)
			}
			if l.TotalScore() != test.ExpectedResult {
				t.Errorf("Score should be %d\n%s", test.ExpectedResult, l.AsString())
			}
		})
	}
}
